set fish_greeting                      # Supresses fish's intro message

### ALIASES ###
# editors
alias em='/usr/bin/emacs -nw'
alias emacs="emacsclient -c -a 'emacs'"

# configs
alias alacrittyconfig='vim ~/.config/alacritty/alacritty.yml'
alias fishconfig='vim ~/.config/fish/config.fish'
alias vimconfig='vim ~/.vimrc'

# package manager
alias sapti='sudo apt install'
alias sapts='sudo apt search'
alias saptu='sudo apt update && sudo apt-get update && sudo apt full-upgrade'

# a better ls
alias ls='exa -al --color=always --group-directories-first'
alias la='exa -a --color=always --group-directories-first'

# checking for my stupid self
alias cp='cp -i'
alias mv='mv -i'
alias rm='rm -i'

# git bare repository for my config files
alias config='/usr/bin/git --git-dir=/home/chetjones/dotfiles --work-tree=/home/chetjones'
alias dotbare="$HOME/.dotbare/dotbare"
export DOTBARE_DIR="$HOME/dotfiles"
export DOTBARE_TREE="$HOME"
export EDITOR="vim"

# Dwarf Fortress
alias dwarf-fortress='~/Downloads/LinuxLNP-0.47.04-r1/startlnp'

# Godot C#
alias godot='~/Downloads/Godot_v3.2.3-stable_mono_x11_64/Godot_v3.2.3-stable_mono_x11.64'


# Other
alias cl='clear'

starship init fish | source
